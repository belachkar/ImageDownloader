# ImageDownloader - Android app

Part of the Coursera course: [Android App Components - Intents, Activities, and Broadcast Receivers]

By: **_Vanderbilt University_**

Original repository link: [ImageDownloader]

<!-- Links -->
[Android App Components - Intents, Activities, and Broadcast Receivers]: https://www.coursera.org/learn/androidapps/home/welcome "Android App Components - Intents, Activities, and Broadcast Receivers"
[ImageDownloader]: https://gitlab.com/belachkar/ImageDownloader "ImageDownloader"
